# install and build hyde
FROM node:12
WORKDIR /hyde
COPY . /hyde
RUN npm install && npm run build


FROM node:12
WORKDIR /hyde

RUN mkdir -p /out

RUN apt-get update && apt-get install -y inotify-tools rsync
ENV DOCKER=true

# copy over the skeleton
COPY ./skeleton /hyde/skeleton
COPY ./templates /hyde/templates

# copy over the run scripts
COPY run.sh /hyde
COPY run-dev.sh /hyde

# copy over the binaries
COPY --from=0 /hyde/node_modules /hyde/node_modules
COPY --from=0 /hyde/dist /hyde/dist

# on build, compile
ONBUILD COPY . /src
ONBUILD RUN /hyde/run.sh /src /out
