# Hyde: a static Angular site generator

This program takes a bunch of markdown files (and other things required
for a website such as assets & HTML layouts) and generates a SPA
(single-page app) using Angular.

## Usage

Create a `pages` directory. In here, you can create whatever pages
you want your site to have. E.g. you would create an index.md file which
would be your home page. Additional pages use the name of the file
(before the .md) as the URL. So for example, the file `page1.md` would
be exposed at `http://mysite/page1`

To build your markdown files into a server, create a `Dockerfile` (not
in the `pages` directory) with this content:

    FROM registry.gitlab.com/chris13524/hyde
    FROM registry.gitlab.com/chris13524/hyde/server

Finally, build it with `docker build -t mysite .` and start the server
with `docker run -it --rm -p 80:80 -p 443:443 -e SERVERNAME=localhost mysite`

## Development

When developing your site, it may be desired to have instant feedback in
your browser about the changes you are making. To automatically build
your site as you make changes, run this Docker command in the root
directory of your site:

    docker run -it --rm -p "[::]:4200:4200" -v $(pwd):/app registry.gitlab.com/chris13524/hyde /hyde/run-dev.sh /app

When the server starts up, you should be able to view your site at
`localhost:4200`. When you make changes to any files (and save them),
the site will automatically rebuild and refresh your browser.

## Alternative builds

Alternatively, if you want to install on an existing server, you can run
these commands:

  - npm run build
  - SRC=path/to/sources node dist/main.js
  - cd app-out; ng build --prod

The build application is available at `app-out/dist/`.
