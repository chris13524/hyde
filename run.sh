#!/usr/bin/env bash

set -e

export SRC=$1
export OUT=$2

cd "$(dirname "$(readlink -f "$0")")"

node ./dist/main.js
cd app-out
npm install
npm run prerender
mkdir -p "$OUT"
cp -r dist/skeleton/browser/* "$OUT"
