const routes: Routes = [
	/*%ROUTES%*/ {
		path: "agencies",
		children: [
			{ path: "", component: PageComponent__agencies_index, children: [] }
		]
	},
	{
		path: "business",
		children: [
			{ path: "", component: PageComponent__business_index, children: [] }
		]
	},
	{
		path: "good",
		children: [
			{ path: "", component: PageComponent__good_index, children: [] }
		]
	},
	{ path: "", component: PageComponent__index, children: [] },
	{ path: "**", component: PageComponent__404, children: [] }
];
