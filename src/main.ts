import { copySync, existsSync, lstatSync, readdirSync, readFileSync, removeSync, writeFileSync, exists, ensureDirSync } from "fs-extra";
import MarkdownIt from "markdown-it";
import sanitize, { Attributes } from "sanitize-html";
import YAML from "yaml";

const yamlFront = require("yaml-front-matter");

const SRC = process.env.SRC;
if (SRC == null) throw new Error("Must set the SRC environment variable.");

const markdownIt = new MarkdownIt({
	html: true
});

function transformTags(attribs: Attributes) {
	for (const attr of ["routerLink", "routerLinkActive", "viewBox", "*ngIf", "*ngFor", "*lazyLoad", "[(ngModel)]", "(ngSubmit)"]) {
		const lowercase = attr.toLowerCase();
		if (attribs.hasOwnProperty(lowercase)) {
			attribs[attr] = attribs[lowercase];
			delete attribs[lowercase];
		}
	}
}

const SANITIZE_OPTIONS: sanitize.IOptions = {
	allowedTags: false, // allow all tags
	allowedAttributes: false, // allow all attributes
	selfClosing: ["img", "br", "hr", "area", "base", "input", "link", "meta"],
	allowedSchemes: ["http", "https", "ftp", "mailto", "tel"],
	allowedSchemesByTag: {},
	allowedSchemesAppliedToAttributes: ["href", "src", "cite"],
	allowProtocolRelative: true,
	transformTags: {
		"*": (tagName: string, attribs: Attributes) => {
			transformTags(attribs);

			return {
				tagName: tagName,
				attribs: attribs
			};
		},
		a: (tagName: string, attribs: Attributes) => {
			transformTags(attribs);

			if (attribs.hasOwnProperty("resource")) {
				delete attribs.resource;
			} else {
				if (!attribs.hasOwnProperty("routerLink") && !attribs.href.match(/^[a-z]+:/)) {
					if (attribs.href.indexOf("#") >= 0) {
						const url = attribs.href.substring(0, attribs.href.indexOf("#"));
						const fragment = attribs.href.substr(attribs.href.indexOf("#") + 1);

						if (url.length > 0) {
							attribs.routerLink = url;
						} else {
							attribs.routerLink = ".";
						}
						attribs.fragment = fragment;
					} else {
						attribs.routerLink = attribs.href;
					}

					delete attribs.href;
				}

				if (!attribs.hasOwnProperty("routerLinkActive")) {
					attribs.routerLinkActive = "_link-active";
				}
			}

			return {
				tagName: tagName,
				attribs: attribs
			};
		}
	}
};

const PAGES_ROOT = SRC + "/pages";
const INDEX_NAME = "index";
const LAYOUT_NAME = "layout";
const FOUR_OH_FOUR_NAME = "404";

// clone skeleton
const SKELETON = "skeleton";
const APP_OUT = "app-out";
removeSync(APP_OUT);
copySync(SKELETON, APP_OUT, {
	filter: src => src.indexOf("node_modules") < 0
});

const COMPONENT_TEMPLATE = readFileSync("templates/component.ts.template", "utf8");
const ROUTING_MODULE_PATH = APP_OUT + "/src/app/app-routing.module.ts";

/// takes a path and converts it into a suitable unique identifier
function pathToId(path: string) {
	return "PageComponent_" + path.replace(/[^a-zA-Z_0-9]/g, "_");
}

function updateFile(path: string, selector: string, addition: string) {
	let routingModule = readFileSync(path, "utf8");
	routingModule = routingModule.replace(selector, selector + addition);
	writeFileSync(path, routingModule);
}

type Route = {
	pathPart: string, // e.g. "appeals"
	id: string | null, // the ID of the component
	children: Route[],
};

type WalkResult = {
	rootRoutes: Route[],
	routes: Route[],
};

function walk(basePath = ""): WalkResult {
	const rootRoutes: Route[] = [];
	const routes: Route[] = [];
	let hasLayout = false;
	let hasFourOhFour = false;

	const files = readdirSync(PAGES_ROOT + basePath);
	for (const file of files) {
		const pathExt = basePath + "/" + file;
		const sourceFile = PAGES_ROOT + pathExt;
		if (lstatSync(sourceFile).isDirectory()) {
			const res = walk(pathExt);
			rootRoutes.push(...res.rootRoutes);
			if (res.routes.length > 0) {
				routes.push({
					pathPart: file,
					id: null,
					children: res.routes,
				});
			}
		} else {
			const name = file.substring(0, file.lastIndexOf(".")); // e.g. "index" or "dashboard"
			const extension = file.substr(file.lastIndexOf(".") + 1); // e.g. "html" or "scss"
			const path = basePath + "/" + name;

			if (extension == "md" || extension == "html") {
				// a page, layout, or template
				const parts: {
					title?: string,
					description?: string,
					no_layout?: boolean,
					variables?: { name: string, type?: string, value?: string }[],
					parameters?: { name: string, type?: string, default?: string }[],
					init?: string,
					displayed?: string,
					destroy?: string,
					host?: { name: string, value: string }[],
					animations?: string[],
					__content: string
				} = yamlFront.loadFront(readFileSync(sourceFile, "utf8"));

				// get the HTML for the component
				let html = "";
				if (extension == "html") {
					html = parts.__content;
				} else if (extension == "md") {
					html = markdownIt.render(parts.__content);
				} else {
					throw new Error(extension);
				}
				html = sanitize(html, SANITIZE_OPTIONS);

				// get the SCSS for the component
				let scss = "";
				const scssFile = PAGES_ROOT + path + ".scss";
				if (existsSync(scssFile)) {
					scss = readFileSync(scssFile, "utf8");
				}

				// render the component
				const id = pathToId(path);
				const filePrefix = id + "_component";
				const MODULE_PATH = APP_OUT + "/src/app/app.module.ts";
				let component = COMPONENT_TEMPLATE;
				component = component.replace(/%FILE_PREFIX%/g, filePrefix);
				component = component.replace(/%ID%/g, id);
				ensureDirSync(APP_OUT + "/src/app/pages/" + basePath);
				const pathFilePrefix = basePath + "/" + filePrefix;
				const componentFilePath = APP_OUT + "/src/app/pages/" + pathFilePrefix + ".ts";
				writeFileSync(componentFilePath, component);
				writeFileSync(APP_OUT + "/src/app/pages/" + pathFilePrefix + ".html", html);
				writeFileSync(APP_OUT + "/src/app/pages/" + pathFilePrefix + ".scss", scss);
				const componentImport = "import { " + id + " } from \"./pages" + pathFilePrefix + "\";";

				updateFile(MODULE_PATH, "/*%IMPORTS%*/", componentImport); // import it into the module
				updateFile(MODULE_PATH, "/*%DECLARATIONS%*/", id + ","); // declare it in the module
				updateFile(ROUTING_MODULE_PATH, "/*%IMPORTS%*/", componentImport); // import it into the router

				// behavior
				if (parts.variables != null) {
					for (const variable of parts.variables) {
						let out = "    " + variable.name;
						if (variable.type == null && variable.value == null) throw new Error("variable " + variable.name + " must have a type or value");
						if (variable.type != null) {
							out += ": " + variable.type;
						}
						if (variable.value != null) {
							out += " = " + variable.value;
						}
						out += ";\n"

						updateFile(componentFilePath, "/*%BODY%*/", out);
					}
				}
				if (parts.init != null) {
					updateFile(componentFilePath, "/*%INIT%*/", '        ' + parts.init + "\n");
				}
				if (parts.displayed != null) {
					updateFile(componentFilePath, "/*%DISPLAYED%*/", '        ' + parts.displayed + "\n")
				}
				if (parts.destroy != null) {
					updateFile(componentFilePath, "/*%DESTROY%*/", '        ' + parts.destroy + "\n");
				}
				if (parts.host != null) {
					for (const host of parts.host) {
						updateFile(componentFilePath, "/*%HOST%*/", '        "' + host.name + '": "' + host.value + '",\n');
					}
				}
				if (parts.animations != null) {
					for (const animation of parts.animations) {
						updateFile(componentFilePath, "/*%ANIMATIONS%*/", animation + ",\n");
					}
				}

				if (name.startsWith("_")) {
					// layout or template
					const templateName = name.substr(1);
					if (templateName == LAYOUT_NAME) {
						// layout
						hasLayout = true;
					} else {
						// template

						// add selector
						updateFile(componentFilePath, "/*%SELECTOR%*/", 'selector: "' + templateName + '",');

						// parameters
						if (parts.parameters != null) {
							for (const param of parts.parameters) {
								let out = "    @Input()" + param.name;
								if (param.type == null && param.default == null) throw new Error("parameter " + param.name + " must have a type or default value");
								if (param.type != null) {
									out += ": " + param.type;
								}
								if (param.default != null) {
									out += " = " + param.default;
								}
								out += ";\n";
								updateFile(componentFilePath, "/*%BODY%*/", out);
							}
						}
					}
				} else if (name == FOUR_OH_FOUR_NAME) {
					// 404 page
					hasFourOhFour = true;
				} else {
					// page

					const title = parts.title || "";
					const description = parts.description || "";
					updateFile(componentFilePath, "/*%INIT%*/", `
						this.title.setTitle("`+ title + `");
						this.meta.updateTag({property: "og:title", content: "`+ title + `"});
						this.meta.updateTag({name: "description", content: "`+ description + `"});
						this.meta.updateTag({property: "og:description", content: "`+ description + `"});
					`);

					const route: Route = {
						pathPart: name == INDEX_NAME ? "" : name,
						id: id,
						children: [],
					};
					if (parts.no_layout) {
						rootRoutes.push(route);
					} else {
						routes.push(route);
					}
				}
			} else {
				// not a page, just include the file in the output
				copySync(PAGES_ROOT + pathExt, APP_OUT + "/src/app/pages" + pathExt);
			}
		}
	}

	// add the 404, if it exists
	if (hasFourOhFour) {
		routes.push({
			pathPart: "**",
			id: pathToId(basePath + "/" + FOUR_OH_FOUR_NAME),
			children: [],
		});
	}

	// wrap everything in a layout, if it exists
	let newRoutes = routes;
	if (hasLayout) {
		// TODO maybe ignore if there are no actual pages?
		newRoutes = [{
			pathPart: "",
			id: pathToId(basePath + "/_" + LAYOUT_NAME),
			children: newRoutes,
		}];
	}

	return {
		rootRoutes,
		routes: newRoutes,
	};
}

const _routes = walk();
const routes = [..._routes.rootRoutes, ..._routes.routes];

function renderRoutes(routes: Route[]): string {
	let rendered = "";
	for (const route of routes) {
		rendered += '{ path:"' + route.pathPart + '"'
			+ (route.id == null ? "" : ", component:" + route.id)
			+ ", children:["
			+ renderRoutes(route.children)
			+ "] },";
	}
	return rendered;
}
updateFile(ROUTING_MODULE_PATH, "/*%ROUTES%*/", renderRoutes(routes))

// read configuration
let config: {
	favicon?: string
} = {};
const CONFIG_YML = SRC + "/config.yml";
if (existsSync(CONFIG_YML)) {
	config = YAML.parse(readFileSync(CONFIG_YML, "utf-8")) || {};
}

// update index.html
const INDEX_HTML = APP_OUT + "/src/index.html";
let indexHtml = readFileSync(INDEX_HTML, "utf8");
const HEAD = "<!--%HEAD%-->";
if (config.favicon) {
	indexHtml = indexHtml.replace(HEAD, "<link rel=\"icon\" href=\"" + config.favicon + "\">" + HEAD);
}
writeFileSync(INDEX_HTML, indexHtml);

// copy over global styles
if (existsSync(SRC + "/global.scss")) {
	copySync(SRC + "/global.scss", APP_OUT + "/src/app/global.scss");
}

// copy over global JavaScript
if (existsSync(SRC + "/global.js")) {
	copySync(SRC + "/global.js", APP_OUT + "/src/app/global.js");
}

// copy over assets
const ASSETS = SRC + "/assets";
if (existsSync(ASSETS)) {
	copySync(ASSETS, APP_OUT + "/src/assets");
}
