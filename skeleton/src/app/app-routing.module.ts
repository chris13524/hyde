import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

/*%IMPORTS%*/

const routes: Routes = [
  /*%ROUTES%*/
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {
      initialNavigation: "enabled"
    })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
