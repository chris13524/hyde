#!/usr/bin/env bash
set -e

export SRC=$1

cd "$(dirname "$(readlink -f "$0")")"

function recompile() {
  (
    set +x
    node dist/main.js
    rsync -rlp --delete app-out/ app-out-serve --exclude=node_modules --exclude=dist
  )
}

recompile

flags=""

if [ -n "$DOCKER" ]; then
  flags="$flags --host 0.0.0.0"
fi

if [ -n "$PUBLIC_HOST" ]; then
  flags="$flags --public-host $PUBLIC_HOST"
fi
echo $flags

export NG_CLI_ANALYTICS=off
(cd app-out-serve && npm install && npm run dev:ssr -- $flags) &

loop=true
while $loop; do
  loop=false
  inotifywait -e modify,create,delete,move -r --exclude .swp$ "$SRC" && recompile && loop=true
done
